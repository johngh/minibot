#!/usr/bin/perl
#
#
#
use warnings;
use strict;

use base qw(Net::Server::PSGI);
use Data::Dumper;
use DBI;
use JSON;
use LWP::UserAgent ();
use Settings;
use base qw(Net::Server::PSGI);

$Data::Dumper::Sortkeys = 1;

my $dbh = 'DBI'->connect("dbi:SQLite:$Settings::db_file","","") or die "Could not connect to DB";
my ($ins, $sel, $message, $me);

my $id_sumbot = $Settings::id_sumbot;

sub DEBUG { 1 }

while (1) {
   # create insert and select statements
   $ins->{user} = eval { $dbh->prepare('INSERT INTO users VALUES (?,?,?)') };
   $ins->{inv} = eval { $dbh->prepare('INSERT INTO inventory VALUES (?,?,?)') };
   $ins->{role} = eval { $dbh->prepare('INSERT INTO role VALUES (?,?,?)') };
   $sel->{user} = eval { $dbh->prepare('SELECT * FROM users where chat_id = ?') };
   $sel->{inv} = eval { $dbh->prepare('SELECT * FROM inventory') };
   $sel->{role} = eval { $dbh->prepare('SELECT * FROM role') };
   # break out of loop if statements prepared
   last if $ins->{user} && $sel->{user} &&
           $ins->{inv} && $sel->{inv} &&
           $ins->{role} && $sel->{role};
}

__PACKAGE__->run(
    port  => [$Settings::port],
    host  => [$Settings::host],
    ipv   => '4', # IPv6 if available
    app => \&my_bot_handler,
);


sub DEBUG { 0 }

sub get_dtg() {
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)
        = gmtime(time);
    return sprintf "%4d-%02d-%02d %02d:%02d:%02d",
        $year + 1900, $mon + 1, $mday, $hour, $min, $sec;
}

sub do_post ($$) {

    my $content = shift;
    my $url = shift;
    DEBUG && print "URL: ", $url, $/;

    my $ua = LWP::UserAgent->new();

    DEBUG && print "Posting: ", Dumper $content;
    my $response = $ua->post( $url, $content );
    my $reply  = $response->decoded_content();

    return $reply;

}


sub sendMessage ($) {
    my $content = shift;
    my $url = "https://api.telegram.org/bot$Settings::token/sendMessage";
    my $response = do_post ($content, $url);
    return $response;
}

sub editMessageText ($) {
    my $content = shift;
    my $url = "https://api.telegram.org/bot$Settings::token/editMessageText";
    my $response = do_post ($content, $url);
    return $response;
}

sub answerCallbackQuery ($) {
    my $content = shift;
    my $url = "https://api.telegram.org/bot$Settings::token/answerCallbackQuery";
    my $response = do_post ($content, $url);
    return $response;
}


sub blank_me () {
    $me = { role => 'unset' };
    $me->{got} = 'blank';
    return $me;
}

sub update_role($) {
    my $role = shift;
    print "Role: $role\n";
}

sub parse_forward ($) {

    my $message = shift;
    my $chat_id = $message->{from}->{id};

    my $reply;

    if ( $message->{forward_from}->{id} == $id_sumbot ) {

        $reply = parse_sumbot($message);

    } else {

        $me = blank_me();

        $reply->{text} = $Settings::msg_not_bot;

        DEBUG && print "Forward not from bot\n";

    }

    my $response = sendMessage({chat_id => $chat_id, text => $reply->{text}, parse_mode => 'html'});
    print Dumper $response;

}

sub parse_command ($) {

    my $message = shift;

    my $chat_id = $message->{from}->{id};

    my $reply;

    if ( $message->{text} eq '/animal' ) {

        # sleep 2;
        $reply->{text} = $Settings::msg_ask_role . $/;
        $me->{role} = 'Asked';

        my $kb_data = {"inline_keyboard" => [
            [
                {"text" => "Octopus","callback_data" => "/oct"},
                {"text" => "Penguin","callback_data" => "/pen"},
                {"text" => "Bushpig","callback_data" => "/pig"}
            ]
        ]};
        $reply->{keyb} = encode_json ($kb_data);

        # my $keyb = '{"keyboard":[["Yes","No"],["Maybe"],["1","2","3"]],"one_time_keyboard":true}';

    } elsif ( $message->{text} eq '/urls' ) {

         $reply->{text} = "Some useful links:";

         my $kb_data = { "inline_keyboard" => [
             [
                 {"text" => "Google", "url" => "http://www.google.com/"},
                 {"text" => "NZ Time", "url" => "https://www.timeanddate.com/worldclock/nz"},
                 {"text" => "Canada Time", "url" => "https://www.timeanddate.com/worldclock/canada"}
             ]
         ] };
         $reply->{keyb} = encode_json ($kb_data);

    } elsif ( $message->{text} eq '/help' ) {

        $reply->{keyb} = "";
        $reply->{text} = "You may have transcended the upper bound of helpability.\n";
       
    } elsif ( $message->{text} eq '/inv' ) {

        $reply->{keyb} = "";
        $reply->{text} = $Settings::msg_ask_inv . $/;

    } elsif ( $message->{text} eq '/tutorial' ) {

        $reply->{keyb} = "";
        $reply->{text} = qq~Once\nI used to be\naddicted to\ncomputer games...\n<br />~ .
            qq~I still am... but my resistance is incredible...<br />~;

    } else {

        $reply->{text} .= "You need to choose an animal...\n";

    }

    $me->{role} = "Hatin" if ! $me->{role};

    if ( $me->{role} eq 'Peck' ) {
        $reply->{keyb} = -1;
    } elsif ( $me->{role} eq 'Oink' ) {
        $reply->{keyb} = -1;
    } elsif ( $me->{role} eq 'Suck' ) {
        $reply->{keyb} = -1;
    } elsif ( $me->{role} eq 'Asked' ) {
        # No /animal message
    } else {
        $reply->{text} .= "Please send /animal then choose your animal.\n";
    }

    if ( $reply->{text} ) {
        print "Responding...\n";
        my $response;
        if ( $reply->{keyb} ) {
            $response = sendMessage({chat_id => $chat_id, text => $reply->{text}, reply_markup => $reply->{keyb}});
        } else {
            $response = sendMessage({chat_id => $chat_id, text => $reply->{text}, parse_mode => 'html'});
        }
        DEBUG && print $response, Dumper decode_json ($response)->{result};
    }

}

sub parse_message($) {

    my $message = shift;

    $sel->{user}->execute($message->{from}->{id});
    my $count = 0;
    while (my @row = $sel->{user}->fetchrow_array) {
         print "$_ ." for @row;
         print $/;
         $count++;
    }

    my ($reply, $keyb);
    if ( ! $count ) { # Don't have user details
        $reply->{text} = "Please send me your /animal\n";
    }

    if ( $message->{forward_from} ) {
        $reply = parse_forward($message);
    } else {
        $reply = parse_command($message);
    }

}

sub parse_callback($) {

    my $callback = shift;

    my $chat_id = $callback->{from}->{id};
    my $chat_name = $callback->{from}->{first_name};
    my $message_id = $callback->{message}->{message_id};
    $me->{id} = $chat_id;

    if ( $callback->{data} eq '/oct' ) {
        $me->{role} = 'Suck';
        update_role('Suck');
    } elsif ( $callback->{data} eq '/pen' ) {
        $me->{role} = 'Peck';
        update_role('Peck');
    } elsif ( $callback->{data} eq '/pig' ) {
        $me->{role} = 'Oink';
        update_role('Oink');
    }

    $sel->{user}->execute($chat_id);
    my $count = 0;
    while (my @row = $sel->{user}->fetchrow_array) {
        print "$_ ." for @row;
        print $/;
        $count++;
    }

    my ($reply_text, $keyb);
    print Dumper $callback;
    $reply_text = "You $me->{role} like it's your nature!";
    if ( ! $count ) { # Don't have user details
        $reply_text = "Have we been introduced my dear?\nPlease send me your /animal\n";
    }
    my $answer = answerCallbackQuery ({chat_id => $chat_id, message_id => $message_id}); 
    my $response = editMessageText ({chat_id => $chat_id, message_id => $message_id, text => $reply_text, parse_mode => 'html'});
    print Dumper $response;

}

sub process_data($) {

    my $json = shift;
    my $postdata = decode_json ($json);

    if ( $postdata->{update_id} ) {
        my $file = "$Settings::logdir/update.$postdata->{update_id}";
        open (my $output_fh, '>', "$file") || die "$0: Can't write to '$file': $!\n";
        my $dtg = get_dtg();
        # print $output_fh $dtg, Dumper $postdata;
        print $output_fh $json;
        print "$dtg $file\n";
        close $output_fh;
    }

    my ($know,$message);
     
    if ( $postdata->{message} ) {
        parse_message( $postdata->{message} );
    } elsif ( $postdata->{callback_query} ) {
        parse_callback( $postdata->{callback_query} );
    } else {
        print Dumper $postdata;
    }

}

sub my_bot_handler {

    my $env = shift;
    my $dtg = get_dtg();

    print "starting:", Dumper $env;

    if ( $env->{'REQUEST_URI'} eq $Settings::uri ) {
        print "GET $env->{'REQUEST_URI'}\n";
    } else {
        print "INVALID URI REQUESTED: [$env->{'REQUEST_URI'}]\n";
    }
    my $count = 0;

    if ( $env->{'REQUEST_METHOD'} eq 'POST' ) {

        use CGI::PSGI;
        my $form = {};
        my $q = CGI::PSGI->new($env);
        $form->{$_} = $q->param($_) for $q->param;
        for my $key ( keys %{$form} ) {
            print "$key : $form->{$key}\n";
        }
        if ( $form->{POSTDATA} ) {
            DEBUG && print "POSTDATA: $form->{POSTDATA}\n";
            process_data($form->{POSTDATA});
            print "---- ---- ---- ----\n";
            return [200, ['Content-type', 'text/plain'], ["OK"]];
        } else {
            print "ERROR: Got no POSTDATA\n";
        }

    } elsif ( $env->{'REQUEST_METHOD'} eq 'GET' ) {
       
        print "GET $env->{'REQUEST_URI'}\n";

    } else {

        print "ERROR: Unrecognized REQUEST_METHOD\n";

        return [500, ['Content-type', 'text/plain'], ["o_O"]];

    }

    print "---- ---- ---- ----\n";

    return [200, ['Content-type', 'text/plain'], [":-p"]];

}

