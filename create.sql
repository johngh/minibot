PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE users (chat_id int, username varchar(255), user varchar(255));
INSERT INTO "users" VALUES(11245678,'MyName','IGID');
CREATE TABLE role (chat_id int, role int, time int);
CREATE TABLE inventory (chat_id int, inventory varchar(255), time int);
COMMIT;
