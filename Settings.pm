package Settings;
use strict; use warnings;
use parent 'Exporter'; # imports and subclasses Exporter

our $token = '123456789:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';

our $uri = '/some-uri';
our $port = 12345;
our $host = 'localhost';
our $logdir = 'logs';
our $id_sumbot = 1234567;
our $db_file = 'storage.db';
our $msg_not_bot = "Dem ain't no bot!";
our $msg_ask_role = "What you doin'?";
our $msg_ask_inv = "Gimme";

our @EXPORT = qw($token $uri $port $host $logdir $id_sumbot $db_file);

1;

